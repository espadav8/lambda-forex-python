import re
import logging
import json
from urllib.request import urlopen
from urllib.parse import urlencode

logger = logging.getLogger()
logger.setLevel(logging.INFO)


def handle(event, context):
    logger.info('Got event {event}'.format(event=event))
    logger.info('Got context {context}'.format(context=context))

    message = do_command(event)
    send_slack_message(event, message)

    return {
        'statusCode': 200
    }


def do_command(event):
    command = get_command(event['event']['text'])
    logger.info('Got raw command: {command}'.format(command=command))

    convert_command = parse_convert_command(command)
    logger.info('Convert command result: {result}'.format(result=convert_command))

    if convert_command:
        return call_fixer(convert_command)

    message = 'I\'m sorry, I don\'t understand the command "{command}". Please use a format like "convert 1AUD to USD"'.format(command=command)

    return message


def get_command(text):
    return re.match('^<@[A-Z0-9]*>(.+)', text).group(1).strip()


def parse_convert_command(command):
    matches = re.match('[a-z\s]*(\d+).*([a-z]{3}).*([a-z]{3})', command, re.IGNORECASE)
    logger.info('Found matches: {matches}'.format(matches=matches))

    if matches:
        return {
            'amount': float(matches[1]),
            'source': matches[2].upper(),
            'target': matches[3].upper()
        }

    return False


def call_fixer(command):
    url = 'https://api.fixer.io/latest?base={source}&symbols={target}'.format(source=command['source'], target=command['target'])

    logger.info('Requesting {url}'.format(url=url))

    r = urlopen(url)
    encoding = r.info().get_content_charset('utf-8')
    data = json.loads(r.read().decode(encoding))

    logger.info('Fixer JSON response: {data}'.format(data=data))

    if 'error' in data and data['error'] == 'Invalid base':
        return 'No rates found for currency {source}'.format(source=command['source'])

    if command['target'] not in data['rates']:
        return 'No rates found for currency {target}'.format(target=command['target'])

    result = float(data['rates'][command['target']]) * command['amount']

    return '{amount}{source} is {result:.2f}{target}'.format(
        amount=command['amount'],
        source=command['source'],
        result=result,
        target=command['target']
    )


def send_slack_message(event, message):
    params = {
        'token': event['team']['bot']['bot_access_token'],
        'channel': event['event']['channel'],
        'text': message
    }

    url = 'https://slack.com/api/chat.postMessage?{params}'.format(params=urlencode(params))

    logger.info('Sending to slack: {url}'.format(url=url))

    r = urlopen(url)
    encoding = r.info().get_content_charset('utf-8')
    data = json.loads(r.read().decode(encoding))

    if data['ok']:
        return True

    return False
