import json
import os
from urllib.parse import urlencode
from urllib.request import urlopen
import logging
import boto3

logger = logging.getLogger()
logger.setLevel(logging.INFO)
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ.get("TEAMS_TABLE"))


def get_code(event):
    code = ''

    if 'queryStringParameters' in event and 'code' in event['queryStringParameters']:
        code = event['queryStringParameters']['code']

    logger.info('Event code: {code}'.format(code=code))

    return code


def request_token(code):
    if code == '':
        return ''

    params = {
        "client_id": os.environ.get("CLIENT_ID"),
        "client_secret": os.environ.get("CLIENT_SECRET"),
        "code": code
    }

    url = "https://slack.com/api/oauth.access?" + urlencode(params)

    logger.info('Sending request to {url}'.format(url=url))

    r = urlopen(url)
    encoding = r.info().get_content_charset('utf-8')
    data = r.read().decode(encoding)

    logger.info('Got response from {url} - {r} - {data}'.format(url=url, r=r, data=data))

    return json.loads(data)


def save_response(response):
    logger.info("Saving to DynamoDB: {item}".format(item=response))

    table.put_item(Item=response)

    return


def handle(event, context):
    try:
        logger.info('Got event {event}'.format(event=event))
        logger.info('Got context {context}'.format(context=context))

        code = get_code(event)
        token_response = request_token(code)
        save_response(token_response)

        return {
            "statusCode": 302,
            "headers": {
                "Location": os.environ.get("INSTALL_SUCCESS_URL")
            }
        }
    except:
        return {
            "statusCode": 302,
            "headers": {
                "Location": os.environ.get("INSTALL_ERROR_URL")
            }
        }
