import json
import os
import logging
import boto3
import re

logger = logging.getLogger()
logger.setLevel(logging.INFO)
aws_lambda = boto3.client('lambda')
dynamodb = boto3.resource('dynamodb')
table = dynamodb.Table(os.environ.get('TEAMS_TABLE'))

actions = {
    'convert': os.environ.get('NAMESPACE') + '-convert',
}


def get_slack_event(event):
    return json.loads(event['body'])


def is_token_valid(event):
    return event['token'] == os.environ.get('VERIFICATION_TOKEN')


def get_team(event):
    key = {
        "team_id": event['team_id']
    }

    logger.info('DynamoDB get_item: {key}'.format(key=key))

    response = table.get_item(Key=key)
    logger.info('DynamoDB got: {response}'.format(response=response))

    item = response['Item']

    return item


def was_bot_mention(event, team):
    message = event['event']['text']
    bot_user_id = team['bot']['bot_user_id']
    bot_mentioned_regex = '^<@{bot_user_id}>.*$'.format(bot_user_id=bot_user_id)

    if re.match(bot_mentioned_regex, message):
        return True

    return False


def invoke_action(event, team):
    action = event['event']['text'].split()[1]

    if action not in actions:
        action = 'convert'

    action = os.environ.get('NAMESPACE') + '-' + action

    logger.info('Invoking {action} with event: {event}'.format(action=action, event=event))

    event['team'] = team

    aws_lambda.invoke(
        InvocationType='Event',
        FunctionName=action,
        Payload=json.dumps(event),
        LogType='None'
    )


def handle(event, context):
    logger.info('Got event {event}'.format(event=event))
    logger.info('Got context {context}'.format(context=context))

    slack_event = get_slack_event(event)

    if slack_event['type'] == 'url_verification':
        return {
            'statusCode': 200,
            'body': slack_event['challenge']
        }

    if not is_token_valid(slack_event):
        return {
            'statusCode': 500
        }

    team = get_team(slack_event)
    if was_bot_mention(slack_event, team):
        invoke_action(slack_event, team)

    return {
        'statusCode': 200
    }
